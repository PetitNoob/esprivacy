#ifndef DEF_MAFENETRE
#define DEF_MAFENETRE

#include <QApplication>
#include <QWidget>
#include <QPushButton>
#include <QTcpSocket>

class MaFenetre : public QWidget { // On hérite de QWidget (IMPORTANT)
    Q_OBJECT
    public:
        MaFenetre();

    private:
        QPushButton *m_bouton;
        QTcpSocket  *socket;
        bool connectionFlag;

        void TCPconnect();
        void TCPdisconnect();

    public slots :
        void connectTCP();
        void donneRecu();
};

#endif
