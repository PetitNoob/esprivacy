#include "MaFenetre.h"

#include <QHostAddress>
#include <QMessageBox>

MaFenetre::MaFenetre() : QWidget() {
    // Changement de la taille de la fenêtre
    setFixedSize(300, 150);

    // Construction du bouton
    m_bouton = new QPushButton("Se connecter à l'ESP32", this);
    m_bouton->move(80, 50);
    m_bouton->resize(150, m_bouton->height());

    // Construction du socket
    socket = new QTcpSocket(this);
    connectionFlag = false;

    // Connection des actions aux fonctions
    connect(m_bouton, SIGNAL(clicked()), this, SLOT(connectTCP()));
    connect(socket, SIGNAL(readyRead()), this, SLOT(donneRecu()));
}

void MaFenetre::TCPconnect() {
    // Connection à l'ESP
    socket->connectToHost(QHostAddress("192.168.0.36"), 23);
    qDebug() << "Connecté avec succès au 192.168.0.36";

    // Mise à jour du bouton
    m_bouton->setStyleSheet("QPushButton{ color: green }");
    m_bouton->setText("(Connecté) Se déconnecter");

    // Et sauvegarde l'état "connecté"
    connectionFlag = true;
}
void MaFenetre::TCPdisconnect() {
    // Fermeture de la connection
    socket->close();

    // Mise à jour du bouton
    m_bouton->setText("Se connecter à l'ESP32");
    m_bouton->setStyleSheet("");

    // Et sauvegarde l'état "déconnecté"
    connectionFlag = false;
}

void MaFenetre::connectTCP() {
    qDebug() << "Bouton appuyé";

    if (!connectionFlag)
        TCPconnect();
    else
        TCPdisconnect();
}

void MaFenetre::donneRecu() {
    QByteArray datas = socket->readAll();

    if (datas[0] == '1') {
        // Alerte l'utilisateur avec une boite de message
        QMessageBox msgBox;
        msgBox.setIcon( QMessageBox::Warning);
        msgBox.setText("Quelqu'un a ouvert la porte \n Auto déconnection de la carte");
        msgBox.raise();
        msgBox.setWindowFlags(Qt::WindowStaysOnTopHint);
        msgBox.exec();

        TCPdisconnect();
    }
}
