#include <WiFi.h> // Bibliothèque du wifi

// Broche du capteur
#define PIN D5

char* ssid = "mon wifi";
char* password =  "mon mot de passe";

IPAddress local_IP(192, 168, 0, 36); // L'addresse IP de l'ESP
IPAddress gateway(192, 168, 1, 1); // Addresse IP locale de la passerelle (celle de la Box)
IPAddress subnet(255, 255, 0, 0);
IPAddress primaryDNS(8, 8, 8, 8);  
IPAddress secondaryDNS(8, 8, 4, 4); 

WiFiServer wifiServer(23);

void setup() {
  // == Configuration du moniteur série, pour le débug ==
  Serial.begin(115200);
  
  // == Configuration du wifi ==

  // Initialisation du wifi...
  if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
    Serial.println("Problème de la configuration STA");
  }
  delay(1000);

  // Connexion à ma box
  WiFi.begin(ssid, password);
  Serial.print("On se connecte au wifi ");
  Serial.println(ssid);

  // On affiche des poinst tant que l'ESP n'est pas connecté
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println(".");
  }

  // Enfin connecté !
  Serial.println("On est connecté avec l'adresse IP :");
  Serial.println(WiFi.localIP());

  // On a plus qu'à démarer le serveur ;)
  wifiServer.begin();
  
  // == Configuration du capteur ==
  pinMode(PIN, INPUT);
}

void loop() {
  WiFiClient client = wifiServer.available();

  if (client) {
    // Tant que le client (= mon ordinateur) est connecté
    while (client.connected()) {
      // Si on détecte que la porte est ouverte, on avertit l'ordinateur en lui envoyant un '1'
      if (digitalRead(PIN))
        client.println('1');

      delay(10);
    }

    // Le client s'est déconnecté
    client.stop();
    Serial.println("Client déconnecté");
  }
}
