# ESPrivacy

Un projet de dispositif pour avertir l'entrée dans ma chambre. Ce projet contient une partie électronique et une partie logicielle sous windows.

Pour recréer ce dispositif chez vous il vous suffit d'imprimer le fichier "ESPrivacy.stl" en 3D puis trouvez une façon de le fixer à votre porte (personellement j'utilise du "scotch scratch" de ce type : [](https://fr.aliexpress.com/item/4000208669247.html?spm=a2g0o.productlist.0.0.2602140c1Oi8J3&algo_pvid=f3fa939f-0f89-400e-a5e2-ac97ea8cda64&algo_expid=f3fa939f-0f89-400e-a5e2-ac97ea8cda64-30&btsid=d7a4a0a3-601b-4f6b-bd2f-c16de3e5b964&ws_ab_test=searchweb0_0,searchweb201602_4,searchweb201603_55) )
Ensuite une fois le boitier fixé à votre porte Mettez dans le boitier une carte ESP8266 et branchez un câbles sur la pin 3v et un autre sur la pin GND (n'oubliez pas de mettre une résistance sur un des câbles pour éviter de provoquer un court circuit 😉).
Puis téléverser le fichier "ESPrivacy.ino" dans la carte.
Alimentez la carte avec une batterie (je recommande une batterie portable pour téléphone si vous en avez une qui tient dans le boitier).

Maintenant il vous faut installer le logiciel sur votre ordinateur :
Pour cela téléchargez les fichiers dossier "Logiciel Qt" et ouvrez le fichier "preveneur.pro" avec Qt (en faisant bien attention que tous les fichiers soient dans le même dossier).
Voici un lien vers un tutoriel complet (et en français 😉) pour l'installation de Qt : [](http://guillaumebelz.github.io/qtinstall/)

Si vous avez un problème quelconque n'hésitez pas à me contacter : demandez moi en amis sur discord (Mr Moine#0151) c'est là que je suis le plus présent.